// RaptorHS.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "message.h"
#include "packetParser.h"
#include <stdio.h>
#include <stdlib.h>
#define HAVE_STRUCT_TIMESPEC
#include <pthread.h>

using namespace std;
using namespace raptor_hs;

void *task(void *parameter);
void *task(void *parameter)
{
	while (1)
	{
		printf("Hello World\r\n");
	}
	

}

int main()
{
	uint8_t pCheckAlive[] = { 0x00, 0x48, 0x66, 0x46, 0x00, 0x00, 0x00, 0x00 };
	uint8_t pLabelRes[] = { 0x00, 0x52, 0x30, 0x32, 0x00, 0x00, 0x00, 0x06, 0x12, 0x01, 0xAF, 0x01, 0x01, 0x04 };
	uint8_t pRaptorError[] = { 0x00, 0x45, 0x30, 0x31, 0x00, 0x00, 0x00, 0x0D, 0x03 ,0x00 ,0x01 ,0x00 ,0x08 ,0x42 ,0x41 ,0x44 ,0x20 ,0x49 ,0x44 ,0x3D ,0x38 };
	PacketParser myParser;
	int len = 0;
	myParser.Raptor2HostParse(pCheckAlive);
	myParser.Raptor2HostParse(pLabelRes);
	myParser.Raptor2HostParse(pRaptorError);
	uint8_t* buf(nullptr);
	buf = new uint8_t[100];
	memset((void*)buf, 0, 100);
	openSession_t openSeesion;
	openSeesion.messageVersion = 10;
	openSeesion.sessionType = 15;
	len = myParser.packetGenerator(h2r_packet_t::open_session, (void*)&openSeesion, 1, buf);
	for (size_t i = 0; i < len; i++)
	{
		printf("buffer[%d]: 0x%02X\r\n", i, buf[i]);
	}
	printf("\r\n\r\n");
	memset((void*)buf, 0, 100);
	len = myParser.packetGenerator(h2r_packet_t::acknowledgement, nullptr, 3, buf);
	for (size_t i = 0; i < len; i++)
	{
		printf("buffer[%d]: 0x%02X\r\n", i, buf[i]);
	}
	printf("\r\n\r\n");
	memset((void*)buf, 0, 100);
	product_id_t pID = 0x07050301;
	len = myParser.packetGenerator(h2r_packet_t::product_iD_short_form, (void*)&pID, 4, buf);
	for (size_t i = 0; i < len; i++)
	{
		printf("buffer[%d]: 0x%02X\r\n", i, buf[i]);
	}
	printf("\r\n\r\n");
	memset((void*)buf, 0, 100);
	hError_t v;
	v.msg = "BAD ID=7";
	v.length = v.msg.length();
	v.sCode = System_StatusCode_t::invalid_product_id;
	v.status = StatusLevel_t::error;
	len = myParser.packetGenerator(h2r_packet_t::host_error, (void*)&v, 4, buf);
	for (size_t i = 0; i < len; i++)
	{
		printf("buffer[%d]: 0x%02X\r\n", i, buf[i]);
	}
	printf("\r\n\r\n");
	memset((void*)buf, 0, 100);
	label_t l1 = { 10,5,{ 0x38 , 0x32 , 0x37 , 0x33 , 0x34 }},
			l2 = { 1,5,{ 0x31 , 0x32 , 0x33 , 0x34 , 0x39 }};
	labelContents_t lContent;
	lContent.ProductId = 1;
	lContent.NumberOfLabels = 2;
	lContent.labelList.push_back(l1);
	lContent.labelList.push_back(l2);
	len = myParser.packetGenerator(h2r_packet_t::label_contents, (void*)&lContent, 5, buf);
	for (size_t i = 0; i < len ; i++)
	{
		printf("buffer[%d]: 0x%02X\r\n", i, buf[i]);
	}
	printf("\r\n\r\n");
	memset((void*)buf, 0, 100);
	delete buf;
	//pthread_t thread_obj;
	//pthread_attr_t thread_attr;
	//char *First_string = "abc"/*argv[1]*/;
	//pthread_attr_init(&thread_attr);
	//pthread_create(&thread_obj, &thread_attr, task, First_string);
	//pthread_join(thread_obj, NULL);
    return 0;
}

