#pragma once
#include "message.h"
#include <vector>


namespace raptor_hs {
	class eListener
	{
	public:
		virtual void onConfigureResponse(configState_t state, uint8_t sequenceNumber) = 0;
		virtual void onSessionState(sesssionState_t state, uint8_t sequenceNumber) = 0;
		virtual void onLabelResult(results_t result, uint8_t sequenceNumber) = 0;
		virtual void onMsgError(rError_t e, uint8_t sequenceNumber) = 0;
		virtual void onStatusChange(eStatus_t status, uint8_t sequenceNumber) = 0;
		virtual void onAcknowledgement(uint8_t sequenceNumber) = 0;
	};

	class PacketParser {
	public:
		PacketParser();
		~PacketParser();
		bool Raptor2HostParse(uint8_t* buf);
		bool registerListener(eListener* eL);
		bool unregisterListener(eListener* e);
		int packetGenerator(h2r_packet_t type, void* value, uint16_t seqNum, uint8_t* packet);

	private:
		message_header_t parseHead(uint8_t * _array);
		std::vector<eListener*>list;
	};

}

