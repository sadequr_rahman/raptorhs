#include "stdafx.h"
#include "packetParser.h"
#include <stdio.h>
#include <string.h>
#include <algorithm>

namespace raptor_hs {

	uint8_t digit_hexToUint(char digit)
	{
		uint8_t v = (digit > '9') ? (digit &~0x20) - 'A' + 10 : (digit - '0');
		return v;
	}
	uint8_t digit_uintToChar(uint8_t digit) {
		if (digit >= 0 && digit <= 9) {
			return (0x30 + digit);
		}
		return 0x30;
	}
	inline void SwapEndian(uint16_t *val)
	{
		*val = (*val << 8) | (*val >> 8);
	}
	inline uint16_t SwapEndian(uint16_t val)
	{
		return  (val << 8) | (val >> 8);
	}
	inline void SwapEndian(uint32_t *val)
	{
		*val = (*val << 24) | ((*val << 8) & 0x00ff0000) |
			((*val >> 8) & 0x0000ff00) | (*val >> 24);
	}
	inline uint32_t SwapEndian(uint32_t val)
	{
		return (val << 24) | ((val << 8) & 0x00ff0000) |
			((val >> 8) & 0x0000ff00) | (val >> 24);
	}
	inline uint16_t word(uint8_t hb, uint8_t lb) {
		return ((uint16_t)hb << 8) | (0xFF & lb);
	}
	inline uint32_t dWord(uint8_t* _array) {
		uint32_t t(0);
		t = (uint32_t)(_array[0]) << 24 | (uint32_t)(_array[1]) << 16 | (uint16_t)(_array[2]) << 8 | (_array[3]);
		return t;
	}
	PacketParser::PacketParser() {

	}
	PacketParser::~PacketParser(){
	}
	message_header_t PacketParser::parseHead(uint8_t * buf)
	{
		message_header_t _head = { 0 , 0, 0 };
		if (buf[MSG_START_IDX] == MSG_START_BYTE)
		{
			_head.messageType = buf[Offsets::head_msg_type];
			_head.sequenceNumber = (digit_hexToUint(buf[Offsets::head_seq_offset]) * 16 ) + digit_hexToUint(buf[Offsets::head_seq_offset + 1]);
			_head.messageLength = dWord(&buf[Offsets::head_len_offset]);
		}
		return _head;
	}
	bool PacketParser::Raptor2HostParse(uint8_t * buf)
	{
		message_header_t _head = parseHead(buf);
		/*printf("Msg Type: %d\r\n",_head.messageType);
		printf("Msg Length: %d\r\n", _head.messageLength);
		printf("Msg Sequence number: %d\r\n", _head.sequenceNumber);*/
		switch (static_cast<Raptor2Host_Message_t>(_head.messageType))
		{
		case  Raptor2Host_Message_t::CheckAlive:
			printf("CheckAlive\r\n");
			// TO-DO send ACK message
			break;
		case  Raptor2Host_Message_t::ConfigureResponse:
			{
				printf("ConfigureResponse\r\n");
				configState_t s = static_cast<configState_t>(buf[Offsets::ConfigureState]);
				for (auto e : list) {
					e->onConfigureResponse(s,_head.sequenceNumber);
				}
			}
			break;
		case  Raptor2Host_Message_t::LabelResults:
			{
				printf("LabelResults\r\n");
				results_t res;
				res.id = dWord(&buf[Offsets::LabelerID]);
				res.state = static_cast<LabelState_t>(buf[Offsets::LabelState]);
				res.reasonCode = static_cast<DiagnosticsCode_t>(buf[Offsets::ReasonCode]);
				printf("Label Id: %d\t state: %d\t reasonCode: %d\r\n",res.id,res.state,res.reasonCode);
				for (auto e : list) {
					e->onLabelResult(res, _head.sequenceNumber);
				}

			}
		break;
		case  Raptor2Host_Message_t::RaptorError:
			{
				printf("RaptorError\r\n");
				rError_t er;
				er.status = static_cast<StatusLevel_t>(buf[Offsets::StatusLevel]);
				er.e = static_cast<mError_t>(word(buf[Offsets::StatusCode], buf[Offsets::StatusCode+1]));
				er.length = word(buf[Offsets::TextDataLength], buf[Offsets::TextDataLength + 1]);
				er.msg = "";
				for (int i = 0; i < er.length; i++) {
					er.msg += (buf[Offsets::TextData + i]);
				}
				printf("Status Id: %d\t Error Code: %d\t Text Length: %d\t, Text: %s\r\n", er.status, er.e, er.length, er.msg.c_str());
				for (auto e : list) {
					e->onMsgError(er, _head.sequenceNumber);
				}
			}
			break;
		case  Raptor2Host_Message_t::RaptorEventStatus:
			{
				printf("RaptorEventStatus\r\n");
				eStatus_t status;
				status.labelerId = buf[Offsets::LabelerID];
				status.eType = buf[Offsets::EventType];
				status.eState = buf[Offsets::EventState];
				status.eCategory = buf[Offsets::EventCategory];
				status.eId = word(buf[Offsets::StatusId], buf[Offsets::StatusId + 1]);
				status.sType = buf[Offsets::StatusType];
				status.sCount = dWord(&buf[Offsets::StatusCount]);
				status.sScale = buf[Offsets::StatusScale];

				for (auto e : list) {
					e->onStatusChange(status, _head.sequenceNumber);
				}
			}
			break;
		case  Raptor2Host_Message_t::SessionState:
			printf("SessionState\r\n");
			break;
		case  Raptor2Host_Message_t::Acknowledgement:
			printf("Acknowledgement\r\n");
			for (auto e : list) {
				e->onAcknowledgement(_head.sequenceNumber);
			}
			break;
		default:
			printf("Parse/Msg error\r\n");
			break;
		}

		return false;
	}
	bool PacketParser::registerListener(eListener * eL)
	{
		for (auto e : list) {
			if (e == eL) return false;
		}
		list.push_back(eL);
		return true;
	}
	bool PacketParser::unregisterListener(eListener * e)
	{
		auto position = std::find(list.begin(), list.end(), e);
		if (position != list.end())
		{
			list.erase(position);
			return true;
		}
		return false;
	}
	int PacketParser::packetGenerator(h2r_packet_t type, void* value, uint16_t seqNum, uint8_t* packet)
	{
		int len = 0;
		switch (type)
		{
		case h2r_packet_t::open_session:
		{
			message_header_t head; 
			head.messageType = static_cast<uint8_t>(Host2Raptor_Message_t::OpenSession);
			head.sequenceNumber = ((uint16_t)digit_uintToChar(seqNum/10) << 8) | (digit_uintToChar(seqNum % 10) & 0xFF);
			head.messageLength = 2;
			len = MSG_HEAD_SIZE + head.messageLength;
			SwapEndian(&head.messageLength);
			packet[MSG_START_IDX] = MSG_START_BYTE;
			packet[Offsets::head_msg_type] = head.messageType;
			packet[Offsets::head_seq_offset] =  (head.sequenceNumber) >> 8;
			packet[Offsets::head_seq_offset+1] = (head.sequenceNumber & 0xFF);
			memcpy((void*)&packet[Offsets::head_len_offset], (void*)&head.messageLength, sizeof(head.messageLength));
			memcpy((void*)&packet[MSG_HEAD_SIZE], value, 2);
		}
		break;
		case h2r_packet_t::configure_session:
		{
			message_header_t head;
			head.messageType = static_cast<uint8_t>(Host2Raptor_Message_t::ConfigureSession);
			head.sequenceNumber = ((uint16_t)digit_uintToChar(seqNum / 10) << 8) | (digit_uintToChar(seqNum % 10) & 0xFF);
			head.messageLength = 0;
			len = MSG_HEAD_SIZE + head.messageLength;
			SwapEndian(&head.messageLength);
			packet[MSG_START_IDX] = MSG_START_BYTE;
			packet[Offsets::head_msg_type] = head.messageType;
			packet[Offsets::head_seq_offset] = (head.sequenceNumber) >> 8;
			packet[Offsets::head_seq_offset + 1] = (head.sequenceNumber & 0xFF);
			memcpy((void*)&packet[Offsets::head_len_offset], (void*)&head.messageLength, sizeof(head.messageLength));
		}
		break;
		case h2r_packet_t::label_contents:
		{
			labelContents_t *lContent = nullptr;
			lContent = static_cast<labelContents_t*>(value);
			message_header_t head;
			head.messageType = static_cast<uint8_t>(Host2Raptor_Message_t::LabelContents);
			head.sequenceNumber = ((uint16_t)digit_uintToChar(seqNum / 10) << 8) | (digit_uintToChar(seqNum % 10) & 0xFF);
			head.messageLength = 5 + (lContent->NumberOfLabels * 6 ) ;
			for (size_t i = 0; i < lContent->NumberOfLabels; i++)
			{
				head.messageLength += lContent->labelList[i].length;
			}
			len = MSG_HEAD_SIZE + head.messageLength;
			uint32_t pID = 0;
			pID = SwapEndian(lContent->ProductId);
			SwapEndian(&head.messageLength);
			packet[MSG_START_IDX] = MSG_START_BYTE;
			packet[Offsets::head_msg_type] = head.messageType;
			packet[Offsets::head_seq_offset] = (head.sequenceNumber) >> 8;
			packet[Offsets::head_seq_offset + 1] = (head.sequenceNumber & 0xFF);
			memcpy((void*)&packet[Offsets::head_len_offset], (void*)&head.messageLength, sizeof(head.messageLength));
			memcpy((void*)&packet[Offsets::ProductId], (void*)&pID, sizeof(lContent->ProductId));
			packet[Offsets::NumberOfLabels] = lContent->NumberOfLabels;
			int cPos = Offsets::LabelIdentifier;
			for (size_t i = 0; i < lContent->NumberOfLabels; i++)
			{
				uint16_t id = SwapEndian(lContent->labelList[i].id);
				uint32_t len = SwapEndian(lContent->labelList[i].length);
				memcpy((void*)&packet[cPos], (void*)&id, sizeof(lContent->labelList[i].id));
				cPos += 2;
				memcpy((void*)&packet[cPos], (void*)&len, sizeof(lContent->labelList[i].length));
				cPos += 4;
				for (size_t j = 0; j < lContent->labelList[i].length; j++)
				{
					packet[cPos] = lContent->labelList[i].label[j];
					cPos++;
				}
			}
		}
		break;
		case h2r_packet_t::product_iD_short_form:
		{
			message_header_t head;
			head.messageType = static_cast<uint8_t>(Host2Raptor_Message_t::ProductID);
			head.sequenceNumber = ((uint16_t)digit_uintToChar(seqNum / 10) << 8) | (digit_uintToChar(seqNum % 10) & 0xFF);
			head.messageLength = 4;
			len = MSG_HEAD_SIZE + head.messageLength;
			SwapEndian(&head.messageLength);
			packet[MSG_START_IDX] = MSG_START_BYTE;
			packet[Offsets::head_msg_type] = head.messageType;
			packet[Offsets::head_seq_offset] = (head.sequenceNumber) >> 8;
			packet[Offsets::head_seq_offset + 1] = (head.sequenceNumber & 0xFF);
			memcpy((void*)&packet[Offsets::head_len_offset], (void*)&head.messageLength, sizeof(head.messageLength));
			memcpy((void*)&packet[sizeof(message_header_t)], value, 4);
		}
		break;
		case h2r_packet_t::product_iD_long_form:
			break;
		case h2r_packet_t::end_session:
		{
			message_header_t head;
			head.messageType = static_cast<uint8_t>(Host2Raptor_Message_t::EndSession);
			head.sequenceNumber = ((uint16_t)digit_uintToChar(seqNum / 10) << 8) | (digit_uintToChar(seqNum % 10) & 0xFF);
			head.messageLength = 0;
			len = MSG_HEAD_SIZE + head.messageLength;
			SwapEndian(&head.messageLength);
			packet[MSG_START_IDX] = MSG_START_BYTE;
			packet[Offsets::head_msg_type] = head.messageType;
			packet[Offsets::head_seq_offset] = (head.sequenceNumber) >> 8;
			packet[Offsets::head_seq_offset + 1] = (head.sequenceNumber & 0xFF);
			memcpy((void*)&packet[Offsets::head_len_offset], (void*)&head.messageLength, sizeof(head.messageLength));
		}
		break;
		case h2r_packet_t::host_error:
		{
			hError_t* vPtr = nullptr;
			vPtr = (hError_t*)value;
			message_header_t head;
			head.messageType = static_cast<uint8_t>(Host2Raptor_Message_t::HostError);
			head.sequenceNumber = ((uint16_t)digit_uintToChar(seqNum / 10) << 8) | (digit_uintToChar(seqNum % 10) & 0xFF);
			head.messageLength = 5 + vPtr->length;
			len = MSG_HEAD_SIZE + head.messageLength;
			SwapEndian(&head.messageLength);
			packet[MSG_START_IDX] = MSG_START_BYTE;
			packet[Offsets::head_msg_type] = head.messageType;
			packet[Offsets::head_seq_offset] = (head.sequenceNumber) >> 8;
			packet[Offsets::head_seq_offset + 1] = (head.sequenceNumber & 0xFF);
			packet[Offsets::StatusLevel] = (uint8_t)vPtr->status;
			packet[Offsets::StatusCode] = ((uint16_t)vPtr->sCode) >> 8;
			packet[Offsets::StatusCode + 1] = ((uint16_t)vPtr->sCode) & 0xFF;
			packet[Offsets::TextDataLength] = ((uint16_t)vPtr->length) >> 8;
			packet[Offsets::TextDataLength + 1] = ((uint16_t)vPtr->length) & 0xFF;
			memcpy((void*)&packet[Offsets::head_len_offset], (void*)&head.messageLength, sizeof(head.messageLength));
			memcpy((void*)&packet[Offsets::TextData], vPtr->msg.c_str(), vPtr->length);
		}
		break;
		case h2r_packet_t::acknowledgement:
		{
			message_header_t head;
			head.messageType = static_cast<uint8_t>(Host2Raptor_Message_t::Acknowledgement);
			head.sequenceNumber = ((uint16_t)digit_uintToChar(seqNum / 10) << 8) | (digit_uintToChar(seqNum % 10) & 0xFF);
			head.messageLength = 0;
			len = MSG_HEAD_SIZE + head.messageLength;
			SwapEndian(&head.messageLength);
			packet[MSG_START_IDX] = MSG_START_BYTE;
			packet[Offsets::head_msg_type] = head.messageType;
			packet[Offsets::head_seq_offset] = (head.sequenceNumber) >> 8;
			packet[Offsets::head_seq_offset + 1] = (head.sequenceNumber & 0xFF);
			memcpy((void*)&packet[Offsets::head_len_offset], (void*)&head.messageLength, sizeof(head.messageLength));
		}
		break;
		default:
			break;
		}

		return len;
	}

	
	
}


