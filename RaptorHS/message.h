/*
	Author: Sadequr Rahman Rabby
	Company: Acluster LLC BD

	This file contains the message defination
	as well as all related defination for raptor HS
	communication.
*/
#pragma once
#include <stdint.h>
#include <map>
#include <vector>

#define MSG_HEAD_SIZE	8
#define MSG_START_IDX	0
#define MSG_START_BYTE	0

namespace raptor_hs {
	enum class Host2Raptor_Message_t {
		ConfigureSession = 0x43,
		OpenSession = 0x4F,
		HostError = 0x45,
		LabelContents = 0x4C,
		ProductID = 0x48,
		EndSession = 0x54,
		Acknowledgement = 0x4B
	};
	enum class Raptor2Host_Message_t {
		ConfigureResponse = 0x50,
		SessionState = 0x53,
		CheckAlive = 0x48,
		Barcode = 0x42,
		VerifierMessage = 0x56,
		LabelResults = 0x52,
		RaptorError = 0x45,
		RaptorEventStatus = 0x4E,
		Acknowledgement = 0x4B
	};
	enum Offsets {
		head_msg_type = 1,
		head_seq_offset = 2,
		head_len_offset = 4,
		SessionType = MSG_HEAD_SIZE + 0,
		MessageVersion = MSG_HEAD_SIZE + 1,
		SessionStatus = MSG_HEAD_SIZE + 0,
		ConfigureState = MSG_HEAD_SIZE + 0,
		ProductId = MSG_HEAD_SIZE + 0,
		NumberOfLabels = MSG_HEAD_SIZE + 4,
		LabelIdentifier = MSG_HEAD_SIZE + 5,
		DataLength = MSG_HEAD_SIZE + 7,
		Data = MSG_HEAD_SIZE + 11,
		LabelState = MSG_HEAD_SIZE + 4,
		ReasonCode = MSG_HEAD_SIZE + 5,
		StatusLevel = MSG_HEAD_SIZE + 0,
		StatusCode = MSG_HEAD_SIZE + 1,
		TextDataLength = MSG_HEAD_SIZE + 3,
		TextData = MSG_HEAD_SIZE + 5,
		LabelerID = MSG_HEAD_SIZE + 0,
		EventType = MSG_HEAD_SIZE + 1,
		EventState = MSG_HEAD_SIZE + 2,
		EventCategory = MSG_HEAD_SIZE + 3,
		StatusId = MSG_HEAD_SIZE + 4,
		StatusType = MSG_HEAD_SIZE + 6,
		StatusCount = MSG_HEAD_SIZE + 7,
		StatusScale = MSG_HEAD_SIZE + 11,
	};
	enum class sesssionState_t {
		Operational = 0x01,
		NonOperational = 0x00
	};
	enum class LabelState_t {
		Labeled = 0x01,
		NotLabeled = 0x00
	};
	enum class DiagnosticsCode_t {
		Unknown = 0,
		NoLabelContents = 1,
		HostAppliedNoLabel = 2,
		MissedLabelingOnProduct = 3,
		LateArrivingLabelData = 4
	};
	enum class mError_t {
		no_error_scs = 0,
		bad_mail_piece_id,
		bad_msg_parameter,
		bad_msg_type,
		data_len_error,
		missing_label_msg,
		invalid_label_contents,
		piece_no_label_results,
		exceed_label_limit,
		reserved_for_future_enhancement,
		missing_acknowledgement
	};
	enum class StatusLevel_t {
		logging = 1,
		warning = 2,
		error = 3
	};
	enum class System_StatusCode_t {
		no_error_sys,
		invalid_product_id,
		bad_msg_para_sys,
		bad_msg_type_sys,
		data_len_error_sys,
		missing_barcode_msg,
		missing_verifier_msg,
		Reserved_for_future_enhancement,
		missimg_acknowledgement = 10
	};
	enum class h2r_packet_t {
		open_session,
		configure_session,
		label_contents,
		product_iD_short_form,
		product_iD_long_form,
		end_session,
		host_error,
		acknowledgement
	};
	typedef uint8_t configState_t;
	typedef uint32_t product_id_t;
	typedef struct {
		uint8_t messageType;
		uint16_t sequenceNumber;
		uint32_t messageLength;
	}message_header_t;
	typedef struct {
		uint8_t sessionType;
		uint8_t messageVersion;
	}openSession_t;
	typedef struct {
		uint16_t id;
		uint32_t length;
		std::vector<uint8_t>label;
	}label_t;
	typedef struct {
		uint32_t ProductId;
		uint8_t NumberOfLabels;
		std::vector<label_t> labelList;
	}labelContents_t;
	typedef struct {
		uint32_t ProductId;
		uint8_t NumberOfLabels;
		std::vector<uint32_t> DataLength;
		std::map<uint16_t, std::vector<uint8_t> >label;
	}productId_long_t;
	typedef struct {
		uint32_t id;
		LabelState_t state;
		DiagnosticsCode_t reasonCode;
	}results_t;
	typedef struct {
		StatusLevel_t status;
		mError_t e;
		uint16_t length;
		std::string msg;
	}rError_t;
	typedef struct{
		StatusLevel_t status;
		System_StatusCode_t sCode;
		uint16_t length;
		std::string msg;
	}hError_t;
	typedef struct {
		uint8_t labelerId;
		uint8_t eType;
		uint8_t eState;
		uint8_t eCategory;
		uint16_t eId;
		uint8_t sType;
		uint32_t sCount;
		uint8_t sScale;
	}eStatus_t;

}